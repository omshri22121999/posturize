import time
from cv2 import aruco
import cv2
cap=cv2.VideoCapture(1)
def marker(cap,mid):
	aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
	parameters = aruco.DetectorParameters_create()
	ret,frame= cap.read()
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	ids = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)[1]
	if ids is not None:
		if mid in ids:
			return True
		else:
			return False    
	else:
		return False
    
while(True):#reduce delay
	marker_flag=0
	if marker(cap,4):
		with open("bp_final_status.txt","w") as f:
			f.write('3')
		pass
	else:
		time.sleep(0.5)
		print("WAIT")
		if not marker(cap,4):
			time.sleep(0.5)
			if not marker(cap,4):
				marker_flag=1
	if marker_flag==1:
		ret,frame= cap.read()
		cv2.imwrite("marker_final_image.jpg",frame[163:306,350:593])
		break
cap.release()

