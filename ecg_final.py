import subprocess
import numpy as np
main_bool=True
count=0
count_bool=True
while(main_bool and count_bool):
	subprocess.call("./marker_bash_for_ecg.sh")
	subprocess.call("./openpose_bash_ecg.sh")
	f = open("/home/pruthvirg/openpose/build/examples/tutorial_api_python/left_wrist_key_point.txt")
	txt = f.read()
	txt_values = txt.split()
	right_center_x = 75
	right_center_y = 355
	left_center_x = 317.5
	left_center_y = 416.75
	x1 = float(txt_values[0])
	y1 = float(txt_values[1])
	x2 = float(txt_values[2])
	y2 = float(txt_values[3])
	line = [0,0,0]
	line[0] = 1
	line[1] = ((left_center_y-right_center_y)/(right_center_x-left_center_x))
	line[2] = ((right_center_x)*(-line[1])) - (right_center_y)
	if ((line[0] * y1)+(line[1]*x1) + line[2]) < 0  and ((line[0] * y1)+(line[1]*x1) + line[2]) < 0 :
		print("wrists Positions are right")
		with open("json_for_ecg.txt","w") as f:
			f.write('4')
		main_bool = False

	else:
		print("wrist position wrong")
		with open("json_for_ecg.txt","w") as f:
			f.write('1')
		count+=1
		main_bool = True
		if count==5:
			count_bool=False
			print("Please try again")
			with open("json_for_ecg.txt","w") as f:
				f.write('5')		
    

