import cv2
import time
from cv2 import aruco
aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
parameters =  aruco.DetectorParameters_create()
#take image as frame code
def marker(frame,mid):
    	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
    	ids = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)[1]
    	if ids is not None:
        	if mid in ids:
        	    return True
        	else:
        	    return False    
    	else:
        	return False
	
