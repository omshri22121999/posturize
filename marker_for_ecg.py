import cv2
import time
from cv2 import aruco
cap = cv2.VideoCapture(2)

aruco_dict = aruco.Dictionary_get(aruco.DICT_6X6_250)
parameters =  aruco.DetectorParameters_create()
def marker(cap,mid1,mid2):#mid1 left
	ret, frame = cap.read()
	gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
	ids = aruco.detectMarkers(gray, aruco_dict, parameters=parameters)[1]
	if ids is not None:
		if (mid1 not in ids) and (mid2 not in ids):
			print("Done")
			return True 
		elif(mid1 in ids) and (mid2 not in ids):
			print("place your left hand properly on the rod")
			with open("json_for_ecg.txt","w") as f:
				f.write('2')		
			return False
		elif(mid2 in ids) and (mid1 not in ids):
			print("place your right hand properly on the rod")
			with open("json_for_ecg.txt","w") as f:
				f.write('3')
			return False
		elif(mid2 in ids) and (mid1 in ids):
			print("please place your hands properly on the rod")
			with open("json_for_ecg.txt","w") as f:
				f.write('1')			
			return False
	else:
		print("Done")
		return True
	
while(True):
	marker_flag=0
	if not marker(cap,9,10):
		time.sleep(0.3)
		pass
	else:
		time.sleep(0.5)
		print("WAIT")
		if marker(cap,9,10):
			time.sleep(0.5)
			if marker(cap,9,10):
				time.sleep(0.5)
				marker_flag=1
	if marker_flag==1:
		ret,frame= cap.read()
		cv2.imwrite("ecg_marker_final_image.jpg",frame)
		break
cap.release()

